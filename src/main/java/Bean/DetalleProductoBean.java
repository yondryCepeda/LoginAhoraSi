package Bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import Entity.Detalle;


@ManagedBean
@RequestScoped
public class DetalleProductoBean {

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("java");
	EntityManager em = emf.createEntityManager();
	
	List<Detalle> lista =new ArrayList();
	
	private int id;
	
	private int id_producto;
	
	private String detalle;
	
	public String nombreProducto() {
			return "detalleProducto.xhtml";
		
	}
	
	public List<Detalle> findAll(){
		List<Detalle> lista =new ArrayList();
		lista = em.createNamedQuery("DetalleProducto.buscarDetalles", Detalle.class)
				.getResultList();
	//	System.out.println("detalles de productos "+lista);
		return lista;
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public int getId_producto() {
		return id_producto;
	}
	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}
	
	public String volver() {
		return "bienvenido.xhtml";
	}
	
	
}
