package Bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import Entity.Usuario;

	/*
 	este bean se utiliza para que un usuario envie un mensaje y poder 
	visualizar la hora y la fecha de cuando se envio el mensaje
	una vez que se detiene el servidor la aplicacion muere 
	pero aun cerrando la pesta�a vemos que la informacion no se pierde
	*/

@ApplicationScoped
@ManagedBean
public class MensajeBean implements Serializable{

	/*lista de la calse mensaje que nos ayudara a guardar
	la informacion del mensaje como el usuario, mensaje y hora*/
	public List<Mensaje> mensajes = new ArrayList<>();
	
	/*objeto de la clase mensaje que contiene la informacion*/
	private Mensaje mensaje = new Mensaje();

	//metodo utilizado para el boton de enviar el cual va a guardar la informacion que se le ingrese
	public void addMensaje() {
		mensajes.add(mensaje);
		mensaje = new Mensaje();
		Collections.sort(mensajes);
		
	}

	public List<Mensaje> getMensajes() {
		return mensajes;
	}

	public void setMensajes(List<Mensaje> mensajes) {
		this.mensajes = mensajes;
	}

	public Mensaje getMensaje() {
		return mensaje;
	}

	public void setMensaje(Mensaje mensaje) {
		this.mensaje = mensaje;
	}
	
	/*clase mensaje que se usara para guardar la informacion
	y que ademas relaciona al mensaje con la hora implementando el comparable<Mensaje>*/
	public static class Mensaje implements Comparable<Mensaje> {
		private String usuario;
		private Calendar momento = Calendar.getInstance();
		private String mensaje;

		public String getUsuario() {
			return usuario;
		}

		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}

		public Calendar getMomento() {
			return momento;
		}

		public void setMomento(Calendar momento) {
			this.momento = momento;
		}

			public String getMensaje() {
			return mensaje;
		}

		public void setMensaje(String mensaje) {
			this.mensaje = mensaje;
		}

			@Override
		public int compareTo(Mensaje o) {
			return this.momento.before(o.momento) ? 1 : -1;
		}

	}
	
}