package Bean;

import java.io.Serializable;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.spi.Context;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpSession;

import com.sun.faces.util.Util;

import Entity.Usuario;

	/*
	 Este SessionScope se utiliza para guardar la session de una persona que se ha loggeado
	 esta session solo puede morir si expira el tiempo o porque se cierra a travez del metodo invalidate
	 */
@ManagedBean
@SessionScoped
public class UsuarioBean implements Serializable {

	private static final long serialVersionUID = 5577896191759998584L;
	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("java");
	EntityManager em = emf.createEntityManager();
	
	
	private String nombre;
	private String clave;
	private String usuario;
	List<Usuario> lista = new ArrayList();
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}	
	public String getUsuario() {
		Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		return us.getNombre();
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String listaUsuarios() {
	
	lista = em.createNamedQuery("Usuario.usuarios").getResultList();
	System.out.println("lista de usuarios disponibles: "+lista);
	return nombre;
	
	
	}
	
	/*este metodo se utiliza para que un usuario por medio de sus credenciales
	  pueda crear una session*/
	public String lol() {	
	
	System.out.println("nombre: "+nombre);
	System.out.println("clave: "+clave);
	
	lista = em.createNamedQuery("Usuario.validarUsuario", Usuario.class)
				.setParameter("nombre", nombre)
				.setParameter("password", clave)
				.getResultList();
		System.out.println("Lista de personas: "+lista);
		
		//condicion en caso de que el nombre de usuario o contraseņa no existan en la base de datos
		if(lista.isEmpty()) {
			FacesMessage fm = new FacesMessage("user or password wrong","Error msg");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, fm);
			
			return "inicio.xhtml";
		
		}else {
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", lista.get(0));
			return "bienvenido?faces-redirect=true"; 	
			
		}
		
		
	}
	//este metodo se utiliza para cerrar la session con el metodo invalidate de HttpSession
	public String cerrar(){
		HttpSession hs = utils.Util.getSession();
		hs.invalidate();
		return "inicio";
}
	
	//este metodo se utiliza para que un usuario ya loggeado no pueda volver a loggearse sin antes cerrar la session
	public void usuarioRegistrado(){
		try {
			Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		if(us != null) {
			System.out.println("Usted ya esta logeado");
			FacesContext.getCurrentInstance().getExternalContext().redirect("bienvenido.xhtml");
		}
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		
	}
	
	//este metodo se utiliza para advertir a un usuario ya loggeado que no puede volver a loggearse sin antes cerrar la session
	public void verificarSession(){
		
		try {
			Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		if(us==null) {
			System.out.println("No tiene los permisos necesarios");
			FacesContext.getCurrentInstance().getExternalContext().redirect("inicio.xhtml");
		}
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}


	public String detalles() {
		System.out.println("se ejecuto el metodo");
		return "detalle.xhtml";

	}
	

}
